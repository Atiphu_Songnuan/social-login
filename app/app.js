const { Application } = require("@nativescript/core");
const { configureOAuthProviders } = require("./auth-service");

configureOAuthProviders();
Application.run({ moduleName: "app-root" });