const { tnsOauthLogin } = require("./auth-service");
const createViewModel = require("./main-view-model").createViewModel;

exports.onNavigatingTo = (args) => {
    const page = args.object;
    page.bindingContext = createViewModel();
};

exports.onGoogleLogin = () => {
    tnsOauthLogin("google");
};
