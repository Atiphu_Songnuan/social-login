const { configureTnsOAuth, TnsOAuthClient } = require("nativescript-oauth2");
const {
    TnsOaProvider,
    TnsOaProviderOptionsGoogle,
    TnsOaProviderGoogle,
} = require("nativescript-oauth2/providers");

let client = null;

exports.configureOAuthProviders = () => {
    const googleProvider = configureOAuthProviderGoogle();
    configureTnsOAuth([googleProvider]);
};

function configureOAuthProviderGoogle() {
    const googleProviderOptions = {
        openIdSupport: "oid-full",
        clientId:
            "196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth.apps.googleusercontent.com",
        redirectUri:
            "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth:/auth",
        urlScheme:
            "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth",
        scopes: ["email"],
    };
    const googleProvider = new TnsOaProviderGoogle(googleProviderOptions);
    return googleProvider;
}

exports.tnsOauthLogin = (providerType) => {
    client = new TnsOAuthClient(providerType);
    client.loginWithCompletion((token, err) => {
        if (err) {
            console.error("loging in somthings went wrong!");
            console.error(err);
        } else {
            console.log(token);
        }
    });
};
